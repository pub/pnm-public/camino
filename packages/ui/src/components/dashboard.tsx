import { FunctionalComponent, defineAsyncComponent, defineComponent, inject, onMounted, ref } from 'vue'
import { useRouter } from 'vue-router'
import { dashboardApiClient } from './dashboard/dashboard-api-client'
import { User, isAdministration, isEntrepriseOrBureauDEtude, isSuper } from 'camino-common/src/roles'
import { entreprisesKey, userKey } from '@/moi'
import { Entreprise } from 'camino-common/src/entreprise'

export const Dashboard = defineComponent({
  setup() {
    const router = useRouter()

    const user = inject(userKey)
    const entreprises = inject(entreprisesKey, ref([]))

    onMounted(async () => {
      if (!isEntrepriseOrBureauDEtude(user) && !isAdministration(user) && !isSuper(user)) {
        router.replace({ name: 'titres' })
      }
    })

    return () => <PureDashboard user={user} entreprises={entreprises.value} />
  },
})

const PureDashboard: FunctionalComponent<{ user: User; entreprises: Entreprise[] }> = props => {
  if (isEntrepriseOrBureauDEtude(props.user)) {
    const PureEntrepriseDashboard = defineAsyncComponent(async () => {
      const { PureEntrepriseDashboard } = await import('@/components/dashboard/pure-entreprise-dashboard')

      return PureEntrepriseDashboard
    })
    const entrepriseIds = props.user.entrepriseIds ?? []

    return <PureEntrepriseDashboard apiClient={dashboardApiClient} user={props.user} entrepriseIds={entrepriseIds} allEntreprises={props.entreprises} />
  } else if (isAdministration(props.user)) {
    const PureAdministrationDashboard = defineAsyncComponent(async () => {
      const { PureAdministrationDashboard } = await import('@/components/dashboard/pure-administration-dashboard')

      return PureAdministrationDashboard
    })

    return <PureAdministrationDashboard apiClient={dashboardApiClient} user={props.user} entreprises={props.entreprises} />
  } else if (isSuper(props.user)) {
    const PureSuperDashboard = defineAsyncComponent(async () => {
      const { PureSuperDashboard } = await import('@/components/dashboard/pure-super-dashboard')

      return PureSuperDashboard
    })

    return <PureSuperDashboard apiClient={dashboardApiClient} user={props.user} />
  }

  return null
}
