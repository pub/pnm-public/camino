export const PAGE_IDS = {
  contenu: { label: 'Contenu', id: 'main' },
  menu: { label: 'Menu', id: 'headerNavigationId' },
  search: { label: 'Recherche', id: 'search-473-input' },
  footer: { label: 'Pied de page', id: 'footer' },
}
