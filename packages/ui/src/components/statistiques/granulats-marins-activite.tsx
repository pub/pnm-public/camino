import { StatistiqueGranulatsMarinsStatAnnee } from 'camino-common/src/statistiques.js'
import { FunctionalComponent } from 'vue'
import styles from './statistiques.module.css'
import { numberFormat } from 'camino-common/src/number'
import { Alert } from '../_ui/alert'

interface Props {
  enConstruction?: boolean
  statistiqueGranulatsMarins: StatistiqueGranulatsMarinsStatAnnee
}

export const GranulatsMarinsActivite: FunctionalComponent<Props> = props => (
  <div id="indicateurs" class="mb-xxl">
    {(props.enConstruction ?? false) ? <Alert class="fr-mb-2v" type="warning" title="Données en cours de collecte et consolidation." /> : null}

    <div class="fr-grid-row">
      <div class="fr-col-12 fr-col-md-6 fr-mb-3w">
        <h3 class="text-center">Production nette en volume</h3>
        {props.statistiqueGranulatsMarins.activitesDeposesQuantite > 3 ? (
          <div>
            <p class={['fr-display--xs', styles['donnee-importante']]}>{numberFormat(props.statistiqueGranulatsMarins.volume)} m³</p>
          </div>
        ) : (
          <div>
            <p class={['fr-display--xs', styles['donnee-importante']]}>-</p>
          </div>
        )}
      </div>
      <div class="fr-col-12 fr-col-md-6 fr-mb-3w">
        <h3 class="text-center">Production nette en masse</h3>
        {props.statistiqueGranulatsMarins.activitesDeposesQuantite > 3 ? (
          <div>
            <p class={['fr-display--xs', styles['donnee-importante']]}>{numberFormat(props.statistiqueGranulatsMarins.masse)} t</p>
          </div>
        ) : (
          <div>
            <p class={['fr-display--xs', styles['donnee-importante']]}>-</p>
          </div>
        )}
      </div>
    </div>
    <div class="fr-grid-row">
      <div class="fr-col-12 fr-col-md-6 fr-mb-3w">
        <h3 class="text-center">Sources des données</h3>
        <p class={['fr-display--xs', styles['donnee-importante']]}>{numberFormat(props.statistiqueGranulatsMarins.activitesDeposesQuantite)}</p>
        <p>Rapports d’activité de production collectés via Camino utilisés pour consolider ces statistiques.</p>
      </div>
      <div class="fr-col-12 fr-col-md-6 fr-mb-3w">
        <h3 class="text-center">Taux de collecte</h3>
        <p class={['fr-display--xs', styles['donnee-importante']]}>{props.statistiqueGranulatsMarins.activitesDeposesRatio} %</p>
        <p>Des rapports d’activité de production attendus ont été déposés par les opérateurs miniers pour consolider ces statistiques.</p>
      </div>
    </div>
  </div>
)
