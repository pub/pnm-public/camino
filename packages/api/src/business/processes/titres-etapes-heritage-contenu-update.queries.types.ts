/** Types generated for queries found in "src/business/processes/titres-etapes-heritage-contenu-update.queries.ts" */
export type Json = null | boolean | number | string | Json[] | { [key: string]: Json };

/** 'GetAllEtapeIdsDb' parameters type */
export type IGetAllEtapeIdsDbParams = void;

/** 'GetAllEtapeIdsDb' return type */
export interface IGetAllEtapeIdsDbResult {
  heritage_contenu: Json | null;
  id: string;
}

/** 'GetAllEtapeIdsDb' query type */
export interface IGetAllEtapeIdsDbQuery {
  params: IGetAllEtapeIdsDbParams;
  result: IGetAllEtapeIdsDbResult;
}

/** 'GetEtapesByDemarcheInternal' parameters type */
export interface IGetEtapesByDemarcheInternalParams {
  demarcheId?: string | null | void;
  titreId?: string | null | void;
}

/** 'GetEtapesByDemarcheInternal' return type */
export interface IGetEtapesByDemarcheInternalResult {
  communes: Json;
  contenu: Json | null;
  date: string;
  demarche_concurrente_public_lecture: boolean;
  demarche_id: string;
  demarche_id_en_concurrence: string | null;
  demarche_ids_consentement: Json;
  demarche_statut_id: string;
  demarche_type_id: string;
  has_titre_from: boolean | null;
  heritage_contenu: Json | null;
  id: string;
  is_brouillon: boolean;
  ordre: number;
  statut_id: string;
  surface: number | null;
  titre_id: string;
  titre_type_id: string;
  type_id: string;
}

/** 'GetEtapesByDemarcheInternal' query type */
export interface IGetEtapesByDemarcheInternalQuery {
  params: IGetEtapesByDemarcheInternalParams;
  result: IGetEtapesByDemarcheInternalResult;
}

