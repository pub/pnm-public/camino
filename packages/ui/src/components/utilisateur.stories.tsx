import { action } from '@storybook/addon-actions'
import { Meta, StoryFn } from '@storybook/vue3'
import { newEntrepriseId } from 'camino-common/src/entreprise'
import { testBlankUser } from 'camino-common/src/tests-utils'
import { PureUtilisateur, Props } from './utilisateur'
import { toUtilisateurId, utilisateurIdValidator } from 'camino-common/src/roles'
import { qgisTokenValidator } from 'camino-common/src/utilisateur'

const meta: Meta<typeof PureUtilisateur> = {
  title: 'Components/Utilisateur',
  // @ts-ignore @storybook/vue3 n'aime pas les composants tsx
  component: PureUtilisateur,
}
export default meta

const deleteUtilisateur = action('deleteUtilisateur')
const updateUtilisateur = action('updateUtilisateur')
const passwordUpdate = action('passwordUpdate')

const apiClientMock: Props['apiClient'] = {
  getUtilisateur: () =>
    Promise.resolve({
      id: toUtilisateurId('id'),
      email: 'email@gmail.com',
      nom: 'nom',
      prenom: 'prenom',
      role: 'super',
      telephone_fixe: null,
      telephone_mobile: null,
    }),
  removeUtilisateur: params => {
    deleteUtilisateur(params)

    return Promise.resolve()
  },
  updateUtilisateur: params => {
    updateUtilisateur(params)

    return Promise.resolve()
  },
  getQGISToken: () => new Promise(resolve => setTimeout(() => resolve({ token: qgisTokenValidator.parse('token123'), url: 'https://google.fr' }), 1000)),
}

export const MySelf: StoryFn = () => (
  <PureUtilisateur
    entreprises={[{ id: newEntrepriseId('id'), nom: 'Entreprise1', legal_siren: null }]}
    user={{ ...testBlankUser, id: toUtilisateurId('id'), role: 'super' }}
    utilisateurId={utilisateurIdValidator.parse('id')}
    passwordUpdate={passwordUpdate}
    apiClient={apiClientMock}
  />
)

export const Loading: StoryFn = () => (
  <PureUtilisateur
    entreprises={[{ id: newEntrepriseId('id'), nom: 'Entreprise1', legal_siren: null }]}
    user={{ ...testBlankUser, id: toUtilisateurId('id'), role: 'super' }}
    utilisateurId={utilisateurIdValidator.parse('id')}
    passwordUpdate={passwordUpdate}
    apiClient={{
      ...apiClientMock,
      getUtilisateur: () => new Promise(() => ({})),
    }}
  />
)

export const error: StoryFn = () => (
  <PureUtilisateur
    entreprises={[{ id: newEntrepriseId('id'), nom: 'Entreprise1', legal_siren: null }]}
    user={{ ...testBlankUser, id: toUtilisateurId('anotherId'), role: 'super' }}
    utilisateurId={utilisateurIdValidator.parse('id')}
    passwordUpdate={passwordUpdate}
    apiClient={{
      ...apiClientMock,
      getUtilisateur: () => Promise.resolve({ message: 'Cassé' }),
    }}
  />
)

export const AnotherUser: StoryFn = () => (
  <PureUtilisateur
    entreprises={[{ id: newEntrepriseId('id'), nom: 'Entreprise1', legal_siren: null }]}
    user={{ ...testBlankUser, id: toUtilisateurId('anotherId'), role: 'super' }}
    utilisateurId={utilisateurIdValidator.parse('id')}
    passwordUpdate={passwordUpdate}
    apiClient={apiClientMock}
  />
)
