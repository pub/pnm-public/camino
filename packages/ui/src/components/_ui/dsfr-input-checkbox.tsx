import { isEventWithTarget } from '@/utils/vue-tsx-utils'
import { isNotNullNorUndefinedNorEmpty } from 'camino-common/src/typescript-tools'
import { defineComponent } from 'vue'
import { JSX } from 'vue/jsx-runtime'

export type Props = {
  id: string
  legend: { main: string | JSX.Element; description?: string }
  size?: 'sm' | 'md'
  disabled?: boolean
  valueChanged: (value: boolean) => void
  initialValue?: boolean | null
}

export const DsfrInputCheckbox = defineComponent<Props>(props => {
  const updateFromEvent = (e: Event) => {
    if (isEventWithTarget(e)) {
      props.valueChanged(e.target.checked)
    }
  }

  return () => (
    <div class={['fr-checkbox-group', props.size === 'sm' ? 'fr-checkbox-group--sm' : null]}>
      <input onInput={updateFromEvent} disabled={props.disabled ?? false} checked={props.initialValue ?? false} name="archive" id={props.id} type="checkbox" />
      <label class="fr-label" for={props.id}>
        {props.legend.main}
        {isNotNullNorUndefinedNorEmpty(props.legend.description) ? <span class="fr-hint-text">{props.legend.description}</span> : null}
      </label>
    </div>
  )
})
// @ts-ignore waiting for https://github.com/vuejs/core/issues/7833
DsfrInputCheckbox.props = ['id', 'initialValue', 'valueChanged', 'legend', 'disabled', 'size']
