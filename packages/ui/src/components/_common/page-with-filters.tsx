import { fr } from '@codegouvfr/react-dsfr'
import { FunctionalComponent } from 'vue'
import type { JSX } from 'vue/jsx-runtime'
type Props = {
  content: JSX.Element
  filtres: JSX.Element
}

export const PageWithFilters: FunctionalComponent<Props> = props => {
  return (
    <div class={fr.cx('fr-container--fluid')} style={{ overflow: 'visible' }}>
      <div class={fr.cx('fr-grid-row')}>
        <div class={fr.cx('fr-col-12', 'fr-col-md-9', 'fr-pt-3w', 'fr-pr-2w', 'fr-pb-3w')} aria-live="polite">
          {props.content}
        </div>
        <div class={fr.cx('fr-col-12', 'fr-col-md-3')} style={{ order: -1 }}>
          {props.filtres}
        </div>
      </div>
    </div>
  )
}
