/// <reference types="vitest" />
import { defineConfig } from 'vite'
import { testEnv } from './test-env'
import path from 'path'
const poolOptions =
  process.env.CI === 'true'
    ? {
        poolOptions: {
          threads: {
            minThreads: 1,
            maxThreads: 5,
          },
        },
      }
    : {}
export default defineConfig({
  test: {
    environment: 'node',
    root: 'src/',
    name: 'integration',
    include: ['**/*.test.integration.ts'],
    setupFiles: path.resolve(__dirname, './tests/vitestSetup.ts'),
    testTimeout: 10000,
    hookTimeout: 45000,
    env: testEnv,
    pool: 'threads',
    ...poolOptions,
  },
})
