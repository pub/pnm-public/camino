import { defineComponent, inject, ref } from 'vue'
import { Liste, Params } from '../_common/liste'
import { Column, TableRow } from '../_ui/table'
import { useRouter } from 'vue-router'
import { GetDemarchesDemarche } from '../titre/demarche-api-client'
import { CaminoFiltre, demarchesDownloadFormats } from 'camino-common/src/filters'
import { getDomaineId, getTitreTypeType } from 'camino-common/src/static/titresTypes'
import { TitresTypesTypes } from 'camino-common/src/static/titresTypesTypes'
import { DemarchesTypes } from 'camino-common/src/static/demarchesTypes'
import { Nom } from '../_common/nom'
import { Domaine as CaminoDomaine } from '../_common/domaine'
import { DemarchesStatuts } from 'camino-common/src/static/demarchesStatuts'
import { TitreStatut } from '../_common/titre-statut'
import { List } from '../_ui/list'
import { ReferencesTypes } from 'camino-common/src/static/referencesTypes'
import { ApiClient, apiClient } from '@/api/api-client'
import { Entreprise } from 'camino-common/src/entreprise'
import { entreprisesKey } from '@/moi'
import { CaminoRouteLocation } from '@/router/routes'
import { CaminoRouter } from '@/typings/vue-router'
import { DemarcheStatut } from '../_common/demarche-statut'

const demarchesColonnes = [
  { id: 'titreNom', contentTitle: 'Titre' },
  { id: 'titreDomaine', contentTitle: '' },
  { id: 'titreType', contentTitle: 'Type de titre' },
  { id: 'titreStatut', contentTitle: 'Statut de titre' },
  { id: 'type', contentTitle: 'Type' },
  { id: 'statut', contentTitle: 'Statut' },
  { id: 'references', contentTitle: 'Références', noSort: true },
] as const satisfies readonly Column[]

type Props = Pick<PureProps, 'travaux' | 'filtres'>

interface PureProps {
  travaux: boolean
  filtres: readonly CaminoFiltre[]
  currentRoute: CaminoRouteLocation
  updateUrlQuery: Pick<CaminoRouter, 'push'>
  apiClient: Pick<ApiClient, 'getDemarches' | 'titresRechercherByNom' | 'getTitresByIds'>
  entreprises: Entreprise[]
}

const demarchesLignesBuild = (demarches: GetDemarchesDemarche[]): TableRow[] =>
  demarches.map(demarche => {
    const domaineId = getDomaineId(demarche.titre.typeId)
    const titreTypeType = TitresTypesTypes[getTitreTypeType(demarche.titre.typeId)]
    const demarcheType = DemarchesTypes[demarche.typeId]
    const columns: TableRow['columns'] = {
      titreNom: { type: 'text', value: demarche.titre.nom },
      titreDomaine: {
        type: 'jsx',
        jsxElement: <CaminoDomaine domaineId={domaineId} />,
        value: domaineId,
      },
      titreType: {
        type: 'jsx',
        jsxElement: <Nom nom={titreTypeType.nom} />,
        value: titreTypeType.nom,
      },
      titreStatut: {
        type: 'jsx',
        jsxElement: <TitreStatut titreStatutId={demarche.titre.titreStatutId} />,
        value: demarche.titre.titreStatutId,
      },
      type: {
        type: 'jsx',
        jsxElement: <Nom nom={demarcheType.nom} />,
        value: demarcheType.nom,
      },
      statut: {
        type: 'jsx',
        jsxElement: <DemarcheStatut demarcheStatutId={demarche.statutId} />,
        value: DemarchesStatuts[demarche.statutId].nom,
      },
      references: {
        type: 'jsx',
        jsxElement: <List elements={demarche.titre.references.map(ref => `${ReferencesTypes[ref.referenceTypeId].nom} : ${ref.nom}`)} mini={true} />,
        value: demarche.titre.references.map(ref => `${ReferencesTypes[ref.referenceTypeId].nom} : ${ref.nom}`),
      },
    }

    return {
      id: demarche.id,
      link: { name: 'titre', params: { id: demarche.titre.slug }, query: { demarcheSlug: demarche.slug } },
      columns,
    }
  })

export const PurePage = defineComponent<PureProps>(props => {
  const getData = async (params: Params<string>) => {
    const demarches = await props.apiClient.getDemarches({
      travaux: props.travaux,
      page: params.page,
      colonne: params.colonne,
      ordre: params.ordre,
      ...params.filtres,
    })

    return { total: demarches.total, values: demarchesLignesBuild(demarches.elements) }
  }

  return () => (
    <Liste
      nom={props.travaux ? 'Travaux' : 'Démarches'}
      colonnes={demarchesColonnes}
      download={{
        downloadTitle: `Télécharger les ${props.travaux ? 'travaux' : 'démarches'}`,
        id: `download${props.travaux ? 'Travaux' : 'Démarches'}`,
        downloadRoute: props.travaux ? '/travaux' : '/demarches',
        formats: demarchesDownloadFormats,
        params: {},
      }}
      renderButton={null}
      listeFiltre={{
        filtres: props.filtres,
        apiClient: props.apiClient,
        updateUrlQuery: props.updateUrlQuery,
        entreprises: props.entreprises,
      }}
      route={props.currentRoute}
      getData={getData}
    />
  )
})

// @ts-ignore waiting for https://github.com/vuejs/core/issues/7833
PurePage.props = ['currentRoute', 'updateUrlQuery', 'apiClient', 'travaux', 'filtres', 'entreprises']

export const Page = defineComponent<Props>(props => {
  const router = useRouter()
  const entreprises = inject(entreprisesKey, ref([]))

  return () => <PurePage filtres={props.filtres} entreprises={entreprises.value} travaux={props.travaux} apiClient={apiClient} currentRoute={router.currentRoute.value} updateUrlQuery={router} />
})

// @ts-ignore waiting for https://github.com/vuejs/core/issues/7833
Page.props = ['travaux', 'filtres']
