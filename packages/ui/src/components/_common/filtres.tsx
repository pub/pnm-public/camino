import { defineComponent, ref } from 'vue'
import { getInitialFiltres } from '../_ui/filters/filters'
import { CaminoFiltre, caminoFiltres } from 'camino-common/src/filters'
import { ApiClient } from '../../api/api-client'
import { Entreprise } from 'camino-common/src/entreprise'
import { CaminoRouteLocation } from '@/router/routes'
import { CaminoRouter } from '@/typings/vue-router'
import { Filters } from '../_ui/filters/filters'

type Params = { [key in Props['filters'][number]]: (typeof caminoFiltres)[key]['validator']['_output'] }
interface Props {
  filters: readonly CaminoFiltre[]
  route: CaminoRouteLocation
  updateUrlQuery: Pick<CaminoRouter, 'push'>
  subtitle?: string
  toggle?: (open: boolean) => void
  paramsUpdate: (params: Params) => void
  apiClient: Pick<ApiClient, 'titresRechercherByNom' | 'getTitresByIds'>
  entreprises: Entreprise[]
}

export const Filtres = defineComponent((props: Props) => {
  const filtresValues = ref(getInitialFiltres(props.route, props.filters))
  const validate = (params: Params) => {
    if (JSON.stringify(filtresValues.value) !== JSON.stringify(params)) {
      // TODO 2024-05-29 ici on clone car, par un hasard étrange, on se retrouve parfois avec la même référence d'objet qui est passée tout le temps, du coup validate est appelé avec la même valeur que filtresValues alors que ça vient de changer...'
      // Il faut qu'on change ce comportement, probablement en faisant en sorte que les filtres utilisent le routeur, et que personne d'autre ne s'en serve pour calculer les filtres initiaux, mais utilisent plutôt les callback des filtres
      const newParams = structuredClone(params)

      filtresValues.value = newParams
      props.paramsUpdate(newParams)
    }
  }

  return () => (
    <Filters
      updateUrlQuery={props.updateUrlQuery}
      route={props.route}
      apiClient={props.apiClient}
      entreprises={props.entreprises}
      filters={props.filters}
      class="flex-grow"
      subtitle={props.subtitle}
      validate={validate}
    />
  )
})

// @ts-ignore waiting for https://github.com/vuejs/core/issues/7833
Filtres.props = ['filters', 'apiClient', 'subtitle', 'toggle', 'paramsUpdate', 'route', 'updateUrlQuery']
