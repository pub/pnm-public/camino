/** Types generated for queries found in "src/api/rest/utilisateurs.queries.ts" */

/** 'UpdateQgisTokenInternal' parameters type */
export interface IUpdateQgisTokenInternalParams {
  qgisToken: string;
  user_id: string;
}

/** 'UpdateQgisTokenInternal' return type */
export type IUpdateQgisTokenInternalResult = void;

/** 'UpdateQgisTokenInternal' query type */
export interface IUpdateQgisTokenInternalQuery {
  params: IUpdateQgisTokenInternalParams;
  result: IUpdateQgisTokenInternalResult;
}

