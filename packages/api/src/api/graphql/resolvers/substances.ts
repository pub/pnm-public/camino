import { SubstanceLegale, SubstancesLegales } from 'camino-common/src/static/substancesLegales'

export const substances = (): Readonly<SubstanceLegale[]> => SubstancesLegales
