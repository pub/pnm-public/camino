import { ITitreEtape } from '../../types'
import { titreDemarcheAnnulationDateFinFind } from './titre-demarche-annulation-date-fin-find'
import { ETAPES_TYPES } from 'camino-common/src/static/etapesTypes'
import { newDemarcheId, newEtapeId } from '../../database/models/_format/id-create'
import { toCaminoDate } from 'camino-common/src/date'
import { describe, expect, test } from 'vitest'
import { ETAPE_IS_NOT_BROUILLON } from 'camino-common/src/etape'
describe("date de fin d'une démarche d'annulation", () => {
  test("retourne la date d'une démarche d'annulation si elle n'a pas de date de fin pour une décision de l'autorité administrative", () => {
    const titreDemarcheAnnulationEtapes: ITitreEtape[] = [
      {
        id: newEtapeId('h-cx-courdemanges-1988-ret01-dex01'),
        titreDemarcheId: newDemarcheId('h-cx-courdemanges-1988-ret01'),
        typeId: ETAPES_TYPES.decisionDeLAutoriteAdministrative,
        statutId: 'acc',
        isBrouillon: ETAPE_IS_NOT_BROUILLON,
        ordre: 1,
        date: toCaminoDate('2013-05-21'),
        concurrence: { amIFirst: true },
        hasTitreFrom: true,
        demarcheIdsConsentement: [],
      },
    ]
    expect(titreDemarcheAnnulationDateFinFind(titreDemarcheAnnulationEtapes)).toEqual('2013-05-21')
  })

  test("retourne la date de fin d'une démarche d'annulation si elle existe pour une décision de l'autorité administrative", () => {
    const titreDemarcheAnnulationEtapesDateFin: ITitreEtape[] = [
      {
        id: newEtapeId('h-cx-courdemanges-1988-ret01-dex01'),
        titreDemarcheId: newDemarcheId('h-cx-courdemanges-1988-ret01'),
        typeId: ETAPES_TYPES.decisionDeLAutoriteAdministrative,
        statutId: 'acc',
        isBrouillon: ETAPE_IS_NOT_BROUILLON,
        ordre: 1,
        date: toCaminoDate('2013-05-21'),
        dateFin: toCaminoDate('2013-05-25'),
        concurrence: { amIFirst: true },
        hasTitreFrom: true,
        demarcheIdsConsentement: [],
      },
    ]
    expect(titreDemarcheAnnulationDateFinFind(titreDemarcheAnnulationEtapesDateFin)).toEqual('2013-05-25')
  })

  test("retourne null si l'étape n'a ni date, ni date de fin", () => {
    // TODO 2022-05-10, c'est étrange, on va à l'encontre de typescript ici. Soit le typage est faux, soit le test ne sert à rien
    const titreDemarcheAnnulationEtapesSansDate: ITitreEtape[] = [
      {
        id: newEtapeId('h-cx-courdemanges-1988-ret01-dex01'),
        titreDemarcheId: newDemarcheId('h-cx-courdemanges-1988-ret01'),
        typeId: 'dex',
        statutId: 'acc',
        ordre: 1,

        // @ts-ignore
        date: null,
      },
    ]
    expect(titreDemarcheAnnulationDateFinFind(titreDemarcheAnnulationEtapesSansDate)).toBeNull()
  })

  test("retourne la date de fin d'une ACO si elle existe", () => {
    const titreDemarcheACOFaitEtapesDateFin: ITitreEtape[] = [
      {
        id: newEtapeId('h-cx-courdemanges-1988-ret01-dex01'),
        titreDemarcheId: newDemarcheId('h-cx-courdemanges-1988-ret01'),
        typeId: 'aco',
        statutId: 'fai',
        isBrouillon: ETAPE_IS_NOT_BROUILLON,
        ordre: 1,
        date: toCaminoDate('2013-05-21'),
        dateFin: toCaminoDate('2013-05-25'),
        concurrence: 'non-applicable',
        hasTitreFrom: 'non-applicable',
        demarcheIdsConsentement: [],
      },
    ]
    expect(titreDemarcheAnnulationDateFinFind(titreDemarcheACOFaitEtapesDateFin)).toEqual('2013-05-25')
  })
})
